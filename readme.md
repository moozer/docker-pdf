docker-pdf
=============

Adding docker images to gitlab.com
----------------

```
# don't show your token to anyone
export CI_TOKEN=asdfghjkl1234

# login to gitlabs docker hub
docker login -u gitlab-ci-token -p $CI_TOKEN registry.gitlab.com

# build and puch it
docker build -t registry.gitlab.com/moozer/pdf-ci .
docker push registry.gitlab.com/moozer/pdf-ci
```

The ci in this repo does it automatically.

gitlab.com has some very good reference material for docker+CI, e.g. [here](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#container-registry-examples)

To use it
----------------

add a line like this in `.gitlab-ci.yml`:

```
...
image: registry.gitlab.com/moozer/docker-pdf:latest
...
```

We currently generate multiple docker instances.

1. `docker-pdf:latest` latest distribution, latest version
2. `docker-pdf/stretch:latest` debian stretch, latest build
3. `docker-pdf/buster:latest` debian buster (ie. stable), latest build
4. `docker-pdf/bulleye:latest` debian bulleseye (ie. testing), latest build

They are automatically rebuild every month - there are some tests, but still stuff might break for you. Please create an issue is that is the case.

There is currently issues with the `linkchecker` package. It is out of testing, so it is omitted from the bullseye image. Go [here](https://tracker.debian.org/pkg/linkchecker) for updates.

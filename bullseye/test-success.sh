#!/bin/bash

set -e

WORKDIR=$(dirname $0)
TESTDIR="$WORKDIR/../test"

cd $TESTDIR

PANDOC_CMD="pandoc --from markdown
       --to latex
       --out test.pdf
       --pdf-engine pdflatex
       test.md"

CONVTOHTML_CMD="/usr/bin/pdftohtml -s test.pdf"
LINKCHECK_CMD="linkchecker -v --check-extern test-html.html"

echo "running command: $PANDOC_CMD"
$PANDOC_CMD

echo "running command: $CONVTOHTML_CMD"
$CONVTOHTML_CMD

#echo "running command: $LINKCHECK_CMD"
#$LINKCHECK_CMD
echo "Not running linkchecking - see readme."
